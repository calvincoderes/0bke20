<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

   <title>Lyfe Admin</title>
   <!-- remove public for error -->
   <base href="{{ url('/') }}" />

  
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ \Asset::cdn('css/bootstrap/bootstrap.min.css') }}" />

    <!-- Font awesome -->
    <link href="{{ \Asset::cdn('css/bootstrap/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet" />

    <!-- Bootstrap 3 Glyphicons -->
    <link href="{{ \Asset::cdn('css/bootstrap/bootstrap-glyphicons.css') }}" type="text/css" rel="stylesheet" />

        <!-- Animate.CSS -->
        <link type="text/css" href="{{ \Asset::cdn('css/animate.css') }}" rel="stylesheet" media="screen" />

    <!-- UTop -->
    <link rel="stylesheet" media="screen,projection" href="{{ \Asset::cdn('js/utop/css/ui.totop.css') }}" />
     <link rel="stylesheet" href="{{ \Asset::cdn('css/style/style.css') }}" />

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="{{ asset('js/jquery.js') }}"></script>    

    <style>
        body {
            font-family: 'sans';
        }
        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="main-body">
  <div class="main-container">
    <div class="col-lg-3"></div>
    <div class="col-lg-6 nm np">
       <div class="body-container">
           
           <!-- content -->
            @yield('content')
        </div>
    </div> 
    <div class="col-lg-3"></div>   
  </div>
  <div class="footer">
  

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!-- end of modal -->
    <!-- JavaScripts -->
     <!-- easing plugin ( optional ) -->
    <script src="{{ asset('js/utop/js/easing.js') }}" type="text/javascript"></script>
    
    <!-- UItoTop plugin -->
    <script src="{{ asset('js/utop/js/jquery.ui.totop.js') }}" type="text/javascript"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('css/bootstrap/bootstrap.min.js') }}"></script>
    
    <!-- App Scripts -->
    <script src="{{ asset('js/select2/js/select2.min.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
  </div>
</body>
</html>
