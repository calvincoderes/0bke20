<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    #use SoftDeletes;

    protected $table = 'post';

    protected $dates = ['deleted_at'];

}
