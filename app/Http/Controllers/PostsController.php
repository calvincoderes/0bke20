<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{
       public $rules =[];
       public $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('posts',$this->data);
    }

    public function validator($data)
    {
      //$data = request
       $validator = \Validator::make($data, $this->rules);
       $msg = $validator->messages(); 



       return $msg; 
    }

    public function createPost()
    {
        $data = [];

        $this->rules = [
            'image'         => 'required|mimes:jpeg,jpg,png',
            'title'         => 'required',
            'description'   => 'required'

        ];

       $validation_error =  $this->validator( \Request::all() );
      if( empty( $validation_error->messages() ) )
      {
         $file = \Request::file('image');
        //save to db
       $insertPost =  Post::insert([
            'title'         => \Request::get('title'),
            'description'   => \Request::get('description'),
            'image'         => $file->getClientOriginalName()
        ]);
       
        $filename = \Image::make($file)->resize(180, 180);

        $filename->save( storage_path() . '/app/images/' . $file->getClientOriginalName() );

        if($insertPost){
            return redirect ('/home');
        }

      }

      //return error
      foreach($validation_error->messages() as $key=>$error)
      {
        session()->flash($key, $error[0]);
      }

      $this->data['old'] = [
        'title' => !empty(\Request::get('title')) ?  \Request::get('title') : '',
        'description' => !empty(\Request::get('description')) ?  \Request::get('description') : '',
      ];  

     return $this->index();
        
    }

    public function deletePost(){

        if( !\Request::ajax() )
            return redirect('/');

        Post::where([ 'id' => \Request::get('post_id') ])->forceDelete();

        return \Response::json( ['success' => 'successfuly deleted' ] );

    }

    public function changePostStatus(){
        if( !\Request::ajax() )
            return redirect('/');

        Post::where([ 'id' => \Request::get('post_id') ])->update([
            'status' => (\Request::get('status') == 'false') ? 'inactive' : 'active'
        ]);

      return \Response::json( ['success' => 'successfuly updated' ] ); 
    }
}
