<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' =>[ 'auth']], function () {

Route::get('/home', 'HomeController@index');

Route::any('/posts', 'PostsController@index');
Route::any('/posts/create', 'PostsController@createPost');
Route::any('/posts/delete', 'PostsController@deletePost');
Route::any('/posts/change_status', 'PostsController@changePostStatus');

Route::get('/', function () {
    return redirect('/home');
});

});


Route::auth();
Route::post('authenticate_user', function(){  

      #dd(\Request::all());

       if( empty(  \Request::get('username') ) ){
          return redirect('login')->with('username_error','Username Field is Required');
        }
        if( empty(  \Request::get('password') ) ){
          return redirect('login')->with('password_error','The Password you entered is incorrect');
        }
       //Request

         $user = array(
                  'username' => \Request::get('username'),
                  'password' => \Request::get('password')
              );
              
              if (\Auth::attempt($user)) {
               return redirect('home')->with('flash_notice', 'You are successfully logged in.');
                  
              }

              // authentication failure! lets go back to the login page
              session()->flash('username_error','Credentials dont match any account.');
              session()->flash('password_error','Credentials dont match any account.');
              return redirect('login');
          });


Route::any('test',function(){
  dd( bcrypt('lyfe2016') );
});