@extends('layouts.app')

@section('content')
<!-- START BODY CONTAINER -->
<div class="login-body-container">

<div class="col-xs-12">

    <div class="row" id="logo" style="padd ing-top:20px;">
        <div class="col-md-3 nm np">
            <a href="<?= url('/') ?>"> <img src="{{ \Asset::cdn('img/lyfe.png') }}" class="img-responsive center" id="home-logo" /> </a>
        </div>
        <div class="col-md-6"></div>
         <div class="col-md-3 nm np"><span class="pull-right"> {{ \Auth::user()->name }} | <a href="{{ url('logout') }}"> Logout</a> </span></div>
    </div>
    <div class="row headbar "></div>
</div> 
<div class="content">
 <div class="row" id="form">
  <div class="col-xs-12">
    <!-- <form class="login-form " role="form" method="POST" action="{{ url('/submit_post') }}"> -->
    {!! Form::open(['url' => '/posts/create', 'class' => '' , 'enctype' => 'multipart/form-data', 'method' => 'POST' ]) !!}

        <div class="form-group">
          <label>Browse Image</label>{!!  Form::file('image', [ 'required']) !!}
        </div>
      <span class="error text-danger">{{ \Session::has('image') ?  \Session::get('image') : '' }}</span>
      <div class="row nm np">
        <div class="col-xs-12 nm np" >
           <div class="form-group nm np required"> 
                {!! Form::text('title', !empty($old['title']) ? $old['title']: '', ['class' => 'form-control astig-form', 'placeholder' => 'Title of your event', 'required' ]) !!}
            <span class="error text-danger error_image error-styles"> {{ \Session::has('username_error') ? \Session::get('username_error') : ''  }}</span> 
           </div> 

        </div>
      </div>

      <!-- image form -->
      <div class="row nm np">
        <div class="col-xs-12 nm np">
           <div class="form-group nm np required"> 
                {!! Form::textarea('description',!empty($old['description']) ? $old['description']: '', ['class' => 'form-control  astig-form', 'placeholder' => 'Type your description here...', 'required' ]) !!}
            <span class="error text-danger error_image error-styles"> {{ \Session::has('username_error') ? \Session::get('username_error') : ''  }}</span> 
           </div> 

        </div>
      </div>


       <div class="row">
        <div class="col-xs-3 pull-right nm np"> 
         <button type="submit" class="btn btn-default col-xs-12" id="submit-button">SUBMIT</button>
        </div>
      </div>

    {!! Form::close()  !!}
    <!-- </form> -->
  </div>
 </div>  
    
</div>
<!-- END OF LOGIN BODY CONTAINER -->
</div>
@endsection
