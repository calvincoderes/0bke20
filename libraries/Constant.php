<?php

class Constant {

	const USER_TYPE			= 2; // Web Content - User Type
	
	const USER_MANAGER		= 'manager';
	const USER_STAFF			= 'staff';
	const USER_CONSULTANT	= 'consultant';
	
	// Archived Status
	const ARCHIVED 			= 1;
	const NOT_ARCHIVED 		= 0;
	
	const STATUS_PENDING	= 1;
	const STATUS_WORKING	= 2;
	const STATUS_DONE		= 3;
	
	// ASSIGNEMENT
	const AC_BASED 				= 'ayala_corporation';
	const SUBS_BASED			= 'subsidiary';

	// Platform Type
	const FSA								= 'FSA';
	const HW								= 'HW';

	const COMMON_ROUTES		= '/,dashboard,/auth/login,/auth/logout';
	
	const SUPER_USER		= 'admin';
	
	//acount status
	const ACCOUNT_ENABLED		= 'enabled';
	const ACCOUNT_DISABLED	= 'disabled';

	const BATCH_CREATED		= 0;
	const BATCH_PENDING		= 1;
	
	
	// Changes Status
	const CHANGES_CREATED	= 0;
	const CHANGES_PENDING	= 1;
	const CHANGES_VALIDATED	= 2;
	const CHANGES_APPROVED	= 3;
	const CHANGES_UPDATED	= 4;
	const CHANGES_DELETED	= 5;
	const CHANGES_REJECTED	= 50;

	// Transfer Price Status List
	const LINK_MAIN_CATEGORY = 'Main Category';
	const LINK_SUB_CATEGORY = 'Category';
	
	// CRUD
	const CRUD_CREATE 	= 'create';
	const CRUD_READ 	= 'read';
	const CRUD_UPDATE	= 'update';
	const CRUD_DELETE	= 'delete';
	
	
	const PENDING = 0;
	const ACTIVATED = 1;
	
	// Approved
	const YES = 1;
	const NO = 0;
		

	// S3 Images Path
	const S3_MERCHANT_IMAGES_PATH 			= '//s3.amazonaws.com/sb-development-rep/img/merchant_product/';
/*
	public static function getVoucherTypes() {
		return [
			self::NON_VOUCHER 					=> 'Non Voucher',
			self::PHYSICAL_VOUCHER 				=> 'Physical Voucher',
			self::SERVICE_VOUCHER				=> 'Service Voucher',
		];
	}*/

	public static function getAssignment($assignment=null){
			$assignment_list = [
					self::AC_BASED		=> 'AC',
					self::SUBS_BASED	=> 'Subsidiary'
			];

			if(!empty($assignment)){
				return $assignment_list[$assignment];
			}else{
				return $assignment_list;
			}
	}

	public static function getChangeStatuses( $prettify = false ) {
		$status = [
				self::CHANGES_CREATED	=> 'Created',
				self::CHANGES_PENDING	=> 'Pending',
				self::CHANGES_VALIDATED	=> 'Validated',
				self::CHANGES_APPROVED	=> 'Approved',				
			];
		
		if ( $prettify ) {
			return array_map(function( $val ) {
				return ucwords(str_replace('_', ' ', $val));
			}, $status);
		}
		
		return $status;		
	}
	
	public static function getUsersLevels( $prettify = false ) {

		$level = [
				self::USER_MANAGER	 => 'manager',
				self::USER_ASSOCIATE => 'associate'
		];
	
		if ( $prettify ) {
			return array_map(function( $val ) {
				return ucwords(str_replace('_', ' ', $val));
			}, $level);
		}
	
		return $level;
	}
	
	
	public static function getYesNo(){
		$param = [
				self::YES	=> 'Yes',
				self::NO	=> 'No'
		];
		return $param;
	}

	
	public static function getLinkFormat(){
		$param = [
				self::LINK_MAIN_CATEGORY => 'Main Category',
				self::LINK_SUB_CATEGORY => 'Category'
		];
		return $param;
	}

	
	public static function getExportTypes(){

		$export_types = [

			self::EXPORT_PRODUCT	=> ['name'				=> 'Product',
										'fields_available'	=> 'name|code|brand|merchant|category|price|transfer_price|quantity|stock_status',
										'filter_available'	=> ['stock_status' => 'All:all|Yes:1|No:0', 'activated' => 'All:all|Yes:1|No:0'] ],

			self::EXPORT_MERCHANT	=> ['name'				=> 'Merchant',
										'fields_available'	=> 'name|code',
										'filter_available'	=> ['name' => 'nice'] ],

			self::EXPORT_BRAND		=> ['name'				=> 'Brand',
										'fields_available'	=> 'name|code',
										'filter_available'	=> ['name' => 'awtsu'] ]
		];

		return $export_types;
	}

	/**
	 * @author Bebe
	 * 10/19/15 02:53PM
	 */
	public static function getChangesHistoryStatus(){

		$changes_status_list = [
			self::CHANGES_CREATED		=> 'Created',
			self::CHANGES_PENDING		=> 'Pending',
			self::CHANGES_VALIDATED		=> 'Validated',
			self::CHANGES_APPROVED		=> 'Approved',
			self::CHANGES_UPDATED		=> 'Updated',
			self::CHANGES_DELETED		=> 'Deleted',
			self::CHANGES_REJECTED		=> 'Rejected',
		];

		return $changes_status_list;
	}
	
}