@extends('layouts.app')

@section('content')
<div class="fixed-header col-xs-12">

    <div class="row" id="logo" style="padding-top:20px;">
        <div class="col-md-3 nm np">
            <a href="<?= url('/') ?>"> <img src="{{ \Asset::cdn('img/lyfe.png') }}" class="img-responsive center" id="home-logo" /> </a>
        </div>
        <div class="col-md-6"></div>
         <div class="col-md-3 nm np"><span class="pull-right"> {{ \Auth::user()->name }} | <a href="{{ url('logout') }}"> Logout</a> </span></div>
    </div>
    <div class="row headbar ">
        <div class="col-xs-3 nm paddingtoponly-35">
            <div class="col-xs-6 np"><label>ACTIVE</label></div>
            <div class="col-xs-6"><label>POSTS</label></div>
        </div>
        <div class="col-xs-6"></div>
        <div class="col-xs-3 nm np"> <a href="{{ url('posts') }}" class="btn btn-default col-xs-12" id="submit-button"><label style="padding-top:7px;">CREATE A POST</label></a></div>
    </div>
</div> 
<div class="content-container">
    
   <div class="scroll-content col-xs-12 np">
      <!--  content repeat  -->
      @foreach($posts as $post)
      <div class="row nm np result-container">
        <div class="col-xs-2 nm np" style="height:100%; border: 1px solid #e5e5e5;">
           <p class="text-center" style="padding-top:90px"> {!! Form::checkbox('active[post_id]','1', ($post->status == 'active') ? true:false, ['class' => 'active-box change-status', 'post-id' => $post->id ]) !!} </p>
        </div>
        <div class="col-xs-10 nm np " style="height:100%; border: 1px solid #e5e5e5; padding-top:10px !important;">
            <div class="col-xs-4 np">
                <div class="post-image"><img src="{{ 'storage/app/images/' . $post->image }}" class="img-responsive center" alt="Image not Found" id="{{$post->id}}-image" onerror="this.onerror=null;this.src='{{ \Asset::cdn('img/placeholder.png') }}';" /></div>
            </div>
            <div class="col-xs-8" style="height:100%">
                <h4 class="post-title" style="color:green;"><label>{{ ucwords($post->title) }}</label></h4>
                <p> Posted {{ $post->created_at }} </p>
                <div class="row" style="position:relative; top:40px;">
                    <div class="col-xs-7"></div>
                    <div class="col-xs-5 nm np"><a class="btn btn-default col-xs-12 delete-post" id="delete-button" post-id="{{ $post->id }}"><label style="padding-top:7px;">DELETE</label></a></div>
                </div>
            </div>
        </div>
      </div>
      @endforeach
      <!-- end of content repeat -->


   </div>
    
</div>
<!-- js -->
<script src="{{ \Asset::cdn('js/jquery/' .'jquery.post.js') }}" type="text/javascript"></script>
@endsection
