
$( document ).ready(function() {

 $('.delete-post').click(function(){
  var postId = $(this).attr('post-id');
   console.log('Post Id selected: '+postId);

   var _body = '';
   var _footer = '';

   _body = '<h3 class="text-center">Are you sure you want to delete this post?</h3>'
   _footer = '<button type="button" class="btn btn-danger prevent confirm-delete">Delete</button> &nbsp; <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'

   $('.modal-title').text('Delete Post');

   $('.modal-body').html(_body);
   $('.modal-footer').html(_footer);

   $('#myModal').modal();

   $('.confirm-delete').click(function(e){
    e.preventDefault();

       /* ajax */
        $.ajax({
            url : $('base').attr('href')+'/posts/delete',
            data : { post_id : postId },
            //method: "POST",
            dataType: 'json',
            headers: { 'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content') },
            success: function (response){
              //

              if(response.error){
                $('.error_groupname').text(response.error);
                return;
              }

              if(response.success){
                location.reload();
              }

              console.log(response.error);
            }

        });

   });


 });


 // checkbox change status
 $('.change-status').bind('change', function() {
      $val = this.checked; //<---
      $postId = $(this).attr('post-id');

       /* ajax */
        $.ajax({
            url : $('base').attr('href')+'/posts/change_status',
            data : { post_id : $postId, 'status' : $val },
            //method: "POST",
            dataType: 'json',
            headers: { 'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content') },
            success: function (response){
              //

              if(response.error){
                $('.error_groupname').text(response.error);
                return;
              }

              if(response.success){
                //location.reload();
              }

              console.log(response.error);
            }

        });



 });






});