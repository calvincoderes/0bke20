@extends('layouts.app')

@section('content')
<div class=" login-container">
    <div class="row" id="logo">
        <div class="col-md-3"></div>
        <div class="col-md-6">
           <a href="<?= url('/') ?>"> <img src="{{ \Asset::cdn('img/lyfe.png') }}" class="img-responsive center nm np" id="login-logo" /> </a>
        </div>
    </div
    <div class="row" id="form">
     <div class="col-xs-12">
    <form class="login-form " role="form" method="POST" action="{{ url('/authenticate_user') }}">
     {!! csrf_field() !!}
      <div class="row">
        <div class="col-xs-12 np">
           <div class="form-group required"> 
                <div class="col-xs-12">
                {!! Form::text('username','', ['class' => 'form-control nm custom-form' ,'placeholder' => 'Username' ]) !!}
                </div>
           </div> 

        </div>
      </div>
      <div class="form-group username-error"  style="min-height:30px;">
       <span class="error text-danger error_username error-styles"> {{ \Session::has('username_error') ? \Session::get('username_error') : ''  }}</span> 
      </div>
    <!-- PASSWORD  -->
    <div class="row">
        <div class="col-xs-12 np">
           <div class="form-group required"> 
                <div class="col-xs-12">
                {!! Form::password('password', ['class' => 'form-control nm custom-form', 'placeholder' => 'Password' ]) !!}
                </div>
           </div> 

        </div>
      </div>
      <div class="form-group password-error"  style="min-height:30px;">
       <span class="error text-danger error_password error-styles">{{ \Session::has('password_error') ? \Session::get('password_error') : ''  }}</span> 
      </div>
      <div class="row">
        <div class="col-xs-4 pull-right nm np"> 
         <button type="submit" class="btn btn-default col-xs-12" id="submit-button">LOG IN</button>
        </div>
      </div>

    </form>
     </div>
    </div>
</div>
@endsection
